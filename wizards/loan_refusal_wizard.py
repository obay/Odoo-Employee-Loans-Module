from openerp import fields, api, models


class EmployeeLoan(models.TransientModel):
    _name = 'employee.loan.refuse.wizard'
    _description = 'Loan Request Refusing Wizard'

    description = fields.Text("Reason for Refusal", required=True)

    @api.multi
    def loan_refuse_reason(self):
        self.ensure_one()

        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        loan = self.env['employee.loan'].browse(active_ids)
        loan.reject_loan(self.description)
        return {'type': 'ir.actions.act_window_close'}
